-- [[ Structired for legendary.nvim's opts.autocmds ]]
-- https://github.com/mrjones2014/legendary.nvim/blob/HEAD/doc/table_structures/AUTOCMDS.md
return {
  {
    { "BufAdd", "BufEnter", "ModeChanged" },
    function()
      local hl = "Conceal"
      hl = "DiagnosticVirtualTextInfo"
      vim.api.nvim_set_hl(0, 'VirtColumn', { link = hl })
    end,
    desc = "Override virtcolumn.nvim's default HL group",
    opts = { pattern = "*.*" },
  },
  {
    { "TextYankPost" },
    vim.highlight.on_yank,
    desc = "Reselect text after yanking",
    opts = { pattern = "*" },
  },
  {
    { "BufAdd" },
    vim.g.pref_tab_order,
    desc = "Sort tabs by " .. vim.g.pref_tab_order,
    opts = { pattern = "*" },
  },
  {
    -- https://github.com/nvim-tree/nvim-tree.lua/wiki/Auto-Close#marvinth01
    { "QuitPre" },
    function()
      local tree_wins = {}
      local floating_wins = {}
      local wins = vim.api.nvim_list_wins()
      for _, w in ipairs(wins) do
        local bufname = vim.api.nvim_buf_get_name(vim.api.nvim_win_get_buf(w))
        if bufname:match("NvimTree_") ~= nil then
          table.insert(tree_wins, w)
        end
        if vim.api.nvim_win_get_config(w).relative ~= '' then
          table.insert(floating_wins, w)
        end
      end
      if 1 == #wins - #floating_wins - #tree_wins then
        -- Should quit, so we close all invalid windows.
        for _, w in ipairs(tree_wins) do
          vim.api.nvim_win_close(w, true)
        end
      end
    end,
    desc = "Do something when quitting??",
    -- opts = {},
  },
}
