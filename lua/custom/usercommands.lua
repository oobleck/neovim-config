local builtin = require "telescope.builtin"
local api = vim.api
local command = api.nvim_create_user_command
local brackets = {
  ['('] = ')',
  ['{'] = '}',
  ['['] = ']',
  ['<'] = '>',
}

command('UpdateAll', function()
  vim.cmd([[bufdo update]])
end, { desc = "Save all unsaved buffers/files" })

-- auto surround word under cursor
function wrap_symbol(opts)
  local args = opts.fargs;
  local current_symbol = vim.call('expand', '<cword>')
  local start_char = args[1]
  if not start_char then
    -- exit without changing the buffer
    return
  end

  local close_char = brackets[start_char] or start_char
  vim.cmd("normal! diwi" .. start_char .. current_symbol .. close_char)
end

command('WrapSymbol', wrap_symbol, { nargs = "?", desc = "Wrap symbol under cursor in a character" })

-- [[ Reload neovim config ]]
-- Inspired by https://stackoverflow.com/a/72504767
function reload_config()
  -- TODO: Get this path dynamically.
  local config_path = "~/.config/nvim/init.lua"
  for name, _ in pairs(package.loaded) do
    if name:match('^user') and not name:match('nvim-tree') then
      package.loaded[name] = nil
    end
  end
  vim.cmd.luafile(config_path)
  vim.notify('Neovim config reloaded')
end

command('ReloadConfig', reload_config, { desc = "Reload nvim config" })
