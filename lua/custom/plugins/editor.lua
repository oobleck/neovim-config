local icons = require('custom.helpers.icons')
local function get_hl(name)
  return vim.api.nvim_get_hl_by_name(name, true)
end

return {
  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',
  {
    -- https://github.com/VidocqH/lsp-lens.nvim#configs
    'VidocqH/lsp-lens.nvim',
    config = true,
  },
  {
    -- https://github.com/anuvyklack/pretty-fold.nvim#foldtext-configuration
    'anuvyklack/pretty-fold.nvim',
    enabled = false,
    dependencies = {
      {
        -- https://github.com/anuvyklack/fold-preview.nvim#configuration
        'anuvyklack/fold-preview.nvim',
        config = true,
        opts = {
          auto = 200,
          border = vim.g.pref_border_style,
        },
      },
    },
    config = true,
    opts = {
      remove_fold_markers = true,
    },
  },
  {
    -- https://github.com/lukas-reineke/indent-blankline.nvim#setup
    -- Add indentation guides even on blank lines
    'lukas-reineke/indent-blankline.nvim',
    main = 'ibl',
    config = function()
      -- See :h ibl.config
      local highlight = {
        'CursorColumn',
        'Whitespace',
      }
      local opts = {
        indent = {
          -- highlight = highlight,
          char = '',
        },
        whitespace = {
          highlight = highlight,
          remove_blankline_trail = true,
        },
        scope = {
          enabled = true,
          -- highlight = highlight,
          show_start = true,
          show_end = true,
          char = '│',
          -- char = "|",
          priority = 500,
        },
      }
      require('ibl').setup(opts)
    end,
  },
  'echasnovski/mini.align',
  'echasnovski/mini.bracketed',
  'echasnovski/mini.bufremove',
  {
    'echasnovski/mini.comment',
    dependencies = {
      { 'JoosepAlviste/nvim-ts-context-commentstring', lazy = true },
    },
    event = 'VeryLazy',
    opts = {
      options = {
        custom_commentstring = function()
          return require('ts_context_commentstring.internal').calculate_commentstring() or vim.bo.commentstring
        end,
      },
    },
  },
  'echasnovski/mini.cursorword',
  'echasnovski/mini.move',
  {
    -- https://github.com/echasnovski/mini.pairs#default-config
    'echasnovski/mini.pairs',
    event = 'VeryLazy',
    config = true,
    opts = {
      mappings = {
        ['('] = { action = 'open', pair = '()', neigh_pattern = '[^\\].' },
        ['['] = { action = 'open', pair = '[]', neigh_pattern = '[^\\].' },
        ['{'] = { action = 'open', pair = '{}', neigh_pattern = '[^\\].' },

        [')'] = { action = 'close', pair = '()', neigh_pattern = '[^\\].' },
        [']'] = { action = 'close', pair = '[]', neigh_pattern = '[^\\].' },
        ['}'] = { action = 'close', pair = '{}', neigh_pattern = '[^\\].' },

        ['"'] = { action = 'closeopen', pair = '""', neigh_pattern = '[^\\].', register = { cr = false } },
        ["'"] = { action = 'closeopen', pair = "''", neigh_pattern = '[^%a\\].', register = { cr = false } },
        ['`'] = { action = 'closeopen', pair = '``', neigh_pattern = '[^\\].', register = { cr = false } },
      },
    },
  },
  {
    -- https://github.com/echasnovski/mini.surround#default-config
    'echasnovski/mini.surround',
    event = 'VeryLazy',
    config = true,
    opts = {
      search_method = 'cover_or_nearest',
    },
  },
  -- 'echasnovski/mini.hlpatterns',
  -- Better text-objects
  {
    'echasnovski/mini.ai',
    enabled = false,
    -- keys = {
    --   { "a", mode = { "x", "o" } },
    --   { "i", mode = { "x", "o" } },
    -- },
    event = 'VeryLazy',
    dependencies = { 'nvim-treesitter-textobjects' },
    opts = function()
      local ai = require 'mini.ai'
      return {
        n_lines = 500,
        custom_textobjects = {
          o = ai.gen_spec.treesitter({
            a = { '@block.outer', '@conditional.outer', '@loop.outer' },
            i = { '@block.inner', '@conditional.inner', '@loop.inner' },
          }, {}),
          f = ai.gen_spec.treesitter({ a = '@function.outer', i = '@function.inner' }, {}),
          c = ai.gen_spec.treesitter({ a = '@class.outer', i = '@class.inner' }, {}),
        },
      }
    end,
    config = function(_, opts)
      require('mini.ai').setup(opts)
      -- register all text objects with which-key
      require('lazyvim.util').on_load('which-key.nvim', function()
        ---@type table<string, string|table>
        local i = {
          [' '] = 'Whitespace',
          ['"'] = 'Balanced "',
          ["'"] = "Balanced '",
          ['`'] = 'Balanced `',
          ['('] = 'Balanced (',
          [')'] = 'Balanced ) including white-space',
          ['>'] = 'Balanced > including white-space',
          ['<lt>'] = 'Balanced <',
          [']'] = 'Balanced ] including white-space',
          ['['] = 'Balanced [',
          ['}'] = 'Balanced } including white-space',
          ['{'] = 'Balanced {',
          ['?'] = 'User Prompt',
          _ = 'Underscore',
          a = 'Argument',
          b = 'Balanced ), ], }',
          c = 'Class',
          f = 'Function',
          o = 'Block, conditional, loop',
          q = 'Quote `, ", \'',
          t = 'Tag',
        }
        local a = vim.deepcopy(i)
        for k, v in pairs(a) do
          a[k] = v:gsub(' including.*', '')
        end

        local ic = vim.deepcopy(i)
        local ac = vim.deepcopy(a)
        for key, name in pairs { n = 'Next', l = 'Last' } do
          i[key] = vim.tbl_extend('force', { name = 'Inside ' .. name .. ' textobject' }, ic)
          a[key] = vim.tbl_extend('force', { name = 'Around ' .. name .. ' textobject' }, ac)
        end
        require('which-key').register {
          mode = { 'o', 'x' },
          i = i,
          a = a,
        }
      end)
    end,
  },
  {
    'abecodes/tabout.nvim',
    dependencies = { 'nvim-treesitter/nvim-treesitter' },
    cinfig = true,
  },
  {
    'ray-x/navigator.nvim',
    enabled = false,
    dependencies = {
      'ray-x/guihua',
      'neovim/nvim-lspconfig',
    },
  },
  {
    -- https://github.com/NvChad/nvim-colorizer.lua#customization
    'NvChad/nvim-colorizer.lua',
    config = true,
    ft = { 'css', 'html', 'markdown' },
    opts = {
      user_default_options = {
        css = true,
        css_fn = true,
        sass = { enable = true, parsers = { 'css' } },
        mode = 'background',
      },
    },
  },
  {
    -- https://github.com/johmsalas/text-case.nvim#all-options-with-their-default-value
    'johmsalas/text-case.nvim',
    dependencies = {
      'nvim-telescope/telescope.nvim',
    },
    config = function()
      require('textcase').setup {}
      require('telescope').load_extension 'textcase'
    end,
    keys = {
      'ga', -- Default invocation prefix
      { 'ga.', '<cmd>TextCaseOpenTelescope<CR>', mode = { 'n', 'x' }, desc = 'Telescope' },
    },
    cmd = {
      -- NOTE: The Subs command name can be customized via the option
      -- "substitude_command_name"
      'Subs',
      'TextCaseOpenTelescope',
      'TextCaseOpenTelescopeQuickChange',
      'TextCaseOpenTelescopeLSPChange',
      'TextCaseStartReplacingCommand',
    },
    -- If you want to use the interactive feature of the `Subs` command right
    -- away, text-case.nvim has to be loaded on startup. Otherwise, the
    -- interactive feature of the `Subs` will only be available after the first
    -- executing of it or after a keymap of text-case.nvim has been used.
    lazy = false,
  },

  -- [[ Cobination rulers styling ]]
  {
    -- https://github.com/xiyaowong/virtcolumn.nvim
    "xiyaowong/virtcolumn.nvim",
    enabled = true,
    event = "BufEnter *.*",
    -- init = function()
    --   vim.g.virtcolumn_char = "┊"
    -- end
  },
  {
    -- https://github.com/fmbarina/multicolumn.nvim#-configuration
    "fmbarina/multicolumn.nvim",
    enabled = true,
    event = "BufEnter *.*",
    config = function()
      local function gitcommit(buf, win)
        local is_first_line = vim.fn.line('.', win) == 1
        local error_hl = get_hl("DiagnosticVirtualTextError")
        return {
          scope = is_first_line and "line" or "window",
          -- TODO: Can I get these rules from a config file?
          rulers = { is_first_line and 51 or 73 },
          to_line_end = true,
          -- bg_color = error_hl.background,
          -- fg_color = error_hl.foreground,
          bg_color = "DarkRed",
          fg_color = "Gold",
        }
      end

      local text_on_dark = "White"
      local text_on_light = "Black"
      local opts = {
        start = "enabled",
        update = "lazy_hold",
        exclude_floating = true,
        exclude_ft = { 'help', 'netrw', 'lazy', 'neotree', 'NeoTree' },
        sets = {
          default = {
            scope = "window",
            rulers = { 81, 121 },
            to_line_end = true,
            bg_color = get_hl("Conceal").background,
            fg_color = get_hl("DiagnosticVirtualTextWarn").foreground,
            -- always_on = true,
          },
          gitcommit = gitcommit,
          NeogitCommitMessage = gitcommit,
        },
      }

      require('multicolumn').setup(opts);
    end,
  },
  {
    "folke/todo-comments.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    event = "BufEnter *.*",
    -- config = true,
    opts = {
      -- TODO: THings
      -- TODO (Spencer) - THings
      -- NOTE: THis is a note
      -- FIX: Fix This
      -- FIXME: Fix this
      -- BUG: There is a vbug in this highlighting?
      -- WARNING: THis is a warning
      -- WARN (spencer): this is a warning
      -- sign_priority = 10,
      highlight = {
        multiline = false,
        multiline_context = 2,
        -- Match slugs followed by a name, or anything before a [:-] delimieter
        pattern = [[.*<(KEYWORDS).*(:|-)]],
      },
      -- keywords = { },
    },
  },
}
