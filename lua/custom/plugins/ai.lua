return {
  {
    -- https://github.com/gsuuon/llm.nvim#configuration
    enabled = false,
    'gsuuon/llm.nvim',
    config = function()
      require('llm').setup({
        -- https://github.com/gsuuon/llm.nvim#prompts
        prompts = vim.tbl_extend('force', require('llm.prompts.starters'), {}),
      })

      -- https://github.com/gsuuon/llm.nvim#-usage
      vim.keymap.set('n', '<leader>LC', '<cmd>LlmCancel<cr>', { desc = '[C]ancel current response' })
      vim.keymap.set('n', '<Leader>Ll', '<cmd>Llm<cr>', { desc = 'Start an [L]lm completion' })
      vim.keymap.set('n', '<Leader>LL', '<cmd>LlmSelect<cr>', { desc = 'Select the response under the cursor' })
      vim.keymap.set('n', '<leader>Ls', '<cmd>LlmShow<cr>',
        { desc = '[S]how the response under the cursor if there is one' })
      vim.keymap.set('n', '<Leader>LD', '<c,d>LlmDelete<cr>', { desc = 'Delete the response under the cursor' })
    end,
  }
}
