return {
  {
    -- https://github.com/windwp/nvim-ts-autotag#override-default-values
    'windwp/nvim-ts-autotag',
    -- Not working correctly yet. Seems ot maybe conflict wth Emmet or something
    -- else.
    enabled = false,
    opts = {
      autotag = {
        enable = true,
        enable_rename = true,
        enable_close = true,
        enable_close_on_slash = true,
      }
    },
  }
}
