return {
  { "dmmulroy/ts-error-translator.nvim" },
  {
    -- https://github.com/pmizio/typescript-tools.nvim#%EF%B8%8F-configuration
    "pmizio/typescript-tools.nvim",
    dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
    ft = { "typescript", "typescript-react", "javascript", "javascript-react" },
    -- lazy = true,
    opts = {
      settings = {
        separate_diagnostic_server = true,
        publish_diagnostic_on = "change",
        -- tsserver_path = "/Users/oobleck/Library/pnpm/tsserver",
        expose_as_code_action = "all",
        complete_function_calls = true,
        code_lens = "references_only",
        disable_member_code_lens = false,
      },
    },
    config = true,
    keys = {
      { "<Leader>cr", vim.lsp.buf.rename, desc = "[R]ename symbol" },
      { "<Leader>co", "<cmd>TSToolsOrganizeImports<cr>",     desc = "[O]rganize Imports" },
      { "<Leader>cs", "<cmd>TSToolsSortImports<cr>",         desc = "[S]ort Impots" },
      { "<Leader>cu", "<cmd>TSToolsRemoveUnusedImports<cr>", desc = "Remove [u]nused imports" },
      { "<leader>cU", "<cmd>TSToolsRemoveUnused<cr>",        desc = "Remove unused statements" },
      { "<Leader>cm", "<cmd>TSToolsAddMissingImports<cr>",   desc = "Add all [m]issing imports" },
      { "<Leader>cF", "<cmd>TSToolsFixAll<cr>",              desc = "Fix all" },
      -- { "gd",         "<cmd>TSToolsGoToSourceDefinition<cr>", desc = "[G]o to source [d]efinition" },
    },
  },
}
