--- Linting partial

return {
  {
    -- https://github.com/mfussenegger/nvim-lint#readme
    'mfussenegger/nvim-lint',
    -- event = 'VeryLazy',
    config = function()
      local linter = require('lint')

      linter.linters_by_ft = {
        markdown = { 'vale' },
        typescript = { 'eslintd' },
        javascript = { 'eslintd' },
      }

      -- vim.api.nvim_create_autocmd({ "BufWritePre" }, {
      --   callback = function()
      --     require('lint').try_lint()
      --   end,
      -- })

      vim.api.nvim_create_user_command('NvimLint', function() linter.try_lint() end, { desc = 'Lint the current buffer' })
      vim.keymap.set('n', '<leader>cl', '<cmd>NvimLint<cr>', { desc = '[L]int the current buffer' })
    end,
  },
}
