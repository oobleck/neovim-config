-- LSP config setup (FOrmatting & liting)

local icons = require('custom.helpers.icons')
local preferred_border = vim.g.pref_border_style or 'single'
local preferred_transparency = vim.g.pref_blend or 0
local diagnostics_format = function(diag)
  if diag.severity == vim.diagnostic.severity.ERROR then
    return icons.diagnostics.Error .. diag.message
  elseif diag.severity == vim.diagnostic.severity.WARN then
    return icons.diagnostics.Warn .. diag.message
  elseif diag.severity == vim.diagnostic.severity.HINT then
    return icons.diagnostics.Hint .. diag.message
  else
    return icons.diagnostics.Info .. diag.message
  end
end

return {
  {
    "nvimtools/none-ls.nvim",
    event = "BufEnter",
    enabled = false,
    dependencies = { "mason.nvim" },
    Xinit = function()
      Util.on_very_lazy(function()
        -- register the formatter with LazyVim
        require("lazyvim.util").format.register({
          name = "none-ls.nvim",
          priority = 200, -- set higher than conform, the builtin formatter
          primary = true,
          format = function(buf)
            return Util.lsp.format({
              bufnr = buf,
              filter = function(client)
                return client.name == "null-ls"
              end,
            })
          end,
          sources = function(buf)
            local ret = require("null-ls.sources").get_available(vim.bo[buf].filetype, "NULL_LS_FORMATTING") or {}
            return vim.tbl_map(function(source)
              return source.name
            end, ret)
          end,
        })
      end)
    end,
    opts = function(_, opts)
      local nls = require("null-ls")
      local null_ls = nls
      opts.root_dir = opts.root_dir
          or require("null-ls.utils").root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git")
      opts.sources = vim.list_extend(opts.sources or {}, {
        nls.builtins.formatting.stylua,
        nls.builtins.formatting.shfmt,
        nls.builtins.code_actions.refactoring,
        nls.builtins.code_actions.gitsigns,
        nls.builtins.completion.luasnip,
        nls.builtins.completion.spell,
        nls.builtins.diagnostics.codespell,
        nls.builtins.diagnostics.commitlint,
        nls.builtins.diagnostics.dotenv_linter,
        nls.builtins.diagnostics.editorconfig_checker,
        nls.builtins.diagnostics.markdownlint_cli2,
        nls.builtins.diagnostics.proselint,
        null_ls.builtins.diagnostics.selene,
        null_ls.builtins.diagnostics.stylelint,
        -- null_ls.builtins.diagnostics.tidy,
        null_ls.builtins.diagnostics.trail_space,
        null_ls.builtins.formatting.biome,
        null_ls.builtins.formatting.mdformat,
        null_ls.builtins.formatting.nixfmt,
        null_ls.builtins.formatting.prettierd,
        -- null_ls.builtins.formatting.shellharden,
        null_ls.builtins.formatting.stylelint,

      })
      opts.border = vim.g.pref_border_style
      opts.log_level = "error"
      return opts
    end,
    config = true,
  },
  {
    -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      {
        'williamboman/mason.nvim',
        config = true,
        opts = {
          ui = {
            border = preferred_border,
            icons = {
              package_installed = "✓",
              package_pending = "➜",
              package_uninstalled = "✗"
            }
          },
        }
      },
      { 'williamboman/mason-lspconfig.nvim' },
      { 'folke/lsp-colors.nvim' },

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { 'j-hui/fidget.nvim',                tag = 'legacy', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
      'b0o/SchemaStore.nvim',
      -- {
      --   -- Configured elsewhere?
      --   "hrsh7th/nvim-cmp",
      -- },
      -- {
      --   "hrsh7th/cmp-nvim-lsp",
      --   cond = function()
      --     return require("lazyvim.util").has("nvim-cmp")
      --   end,
      -- },
      {
        -- https://github.com/rmagatti/goto-preview#%EF%B8%8F-configuration
        'rmagatti/goto-preview',
        opts = {
          default_mappings = true,
          opacity = vim.g.pref_blend or 0,
          dismiss_on_move = false,
        },
        -- keys = {
        --   { 'gp', function() require('goto-preview').goto_preview_definition() end, desc = '[G]o to Definition [P]review' },
        -- },
        -- init = function()
        --   vim.keymap.set("n", "gp", "<cmd>lua require('goto-preview').goto_preview_definition()<CR>",
        --     { noremap = true, desc = '[G]o to Definition [P]review' })
        -- end,
      },
      {
        'nvimdev/lspsaga.nvim',
        event = "LspAttach",
        dependencies = {
          'nvim-treesitter/nvim-treesitter',
          'nvim-tree/nvim-web-devicons',
        },
        config = true,
        keys = {
          { "<Leader>cp", "<cmd>Lspsaga peek_definition<cr>", desc = "[P]eek definition" },
          { "<Leader>cF", "<cmd>Lspsaga finder",              desc = "[F]ind references" },
          { "<Leader>cI", "<cmd>Lspsaga incoming_calls",      desc = "[I]ncoming calls" },
          { "<Leader>cO", "<cmd>Lspsaga outgoing_calls",      desc = "[O]utgoing calls" },
        },
      },

    },
    opts = {
      -- options for vim.diagnostic.config()
      diagnostics = {
        virtual_text = {
          severity = vim.diagnostic.severity.ERROR,
          spacing = 4,
          source = "if_many",
          -- prefix = "◳ ",
          -- this will set set the prefix to a function that returns the diagnostics icon based on the severity
          -- this only works on a recent 0.10.0 build. Will be set to "●" when not supported
          -- prefix = "icons",
          format = diagnostics_format,
        },
        float = {
          source = "if_many", -- Or "always"
          format = diagnmstics_format,
        },

        signs = true,
        underline = true,
        update_in_insert = true,
        severity_sort = true,
      },
      -- Enable the following language servers
      --  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
      --
      --  Add any additional override configuration in the following tables. They will be passed to
      --  the `settings` field of the server config. You must look up that documentation yourself.
      --
      --  If you want to override the default filetypes that your language server will attach to you can
      --  define the property 'filetypes' to the map in question.
      servers = {
        astro = {
          -- init_options = {
          --   typescript = {},
          -- },
        },
        rust_analyzer = {},
        -- tsserver = { filetypes = { 'typescript', 'javascript' } },
        stylelint_lsp = {
          stylelintplus = {
            autoFixOnFormat = true,
          },
        },
        mdx_analyzer = {
          typescript = {
            enabled = true,
          },
        },
        eslint = {},
        bashls = {},
        -- beautysh = {},
        cssls = {},
        marksman = {},
        html = { filetypes = { 'html', 'twig', 'hbs' } },
        jsonls = {
          filetypes = { 'json', 'json5' },
          -- [[ TODO: Restore this once the load sequencing is figured out. ]]
          -- json = {
          --   schemas = require('schemastore').json.schemas(),
          --   validate = { enable = true },
          -- },
        },
        yamlls = { filetypes = { 'yaml', 'docker' } },
        rnix = {},
        nil_ls = {},

        -- OT reqs
        -- ['java-language-server'] = {},
        jdtls = {},

        lua_ls = {
          Lua = {
            workspace = { checkThirdParty = false },
            telemetry = { enable = false },
          },
        },
      },
      setup = {
        -- https://github.com/LazyVim/LazyVim/blob/a72a84972d85e5bbc6b9d60a0983b37efef21b8a/lua/lazyvim/plugins/lsp/init.lua#L79
      },
    },
    config = function(_, opts)
      -- [[ Configure LSP ]]
      local servers = opts.servers

      --  This function gets run when an LSP connects to a particular buffer.
      local on_attach = function(_, bufnr)
        -- NOTE: Remember that lua is a real programming language, and as such it is possible
        -- to define small helper and utility functions so you don't have to repeat yourself
        -- many times.
        --
        -- In this case, we create a function that lets us more easily define mappings specific
        -- for LSP related items. It sets the mode, buffer and description for us each time.
        local nmap = function(keys, func, desc)
          if desc then
            desc = 'LSP: ' .. desc
          end

          vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
        end

        nmap('<leader>cr', vim.lsp.buf.rename, '[R]ename')
        -- nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')
        nmap('<Leader>ca', require("actions-preview").code_actions, '[C]ode [A]ction')
        nmap('<leader>cf', function()
          -- LSP formatter
          vim.cmd([[LspFormat]])

          -- Format by formatter.nvim
          vim.cmd([[Format]])
        end, '[F]ormat current buffer')

        nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
        nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
        nmap('gI', vim.lsp.buf.implementation, '[G]oto [I]mplementation')
        nmap('<leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
        nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
        nmap('<leader>r', require('telescope.builtin').lsp_document_symbols, 'Alias: [D]ocument [S]ymbols')
        nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

        -- See `:help K` for why this keymap
        nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
        nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

        -- Lesser used LSP functionality
        nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
        nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
        nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
        nmap('<leader>wl', function()
          print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, '[W]orkspace [L]ist Folders')

        -- Create a command `:Format` local to the LSP buffer
        vim.api.nvim_buf_create_user_command(bufnr, 'LspFormat', function(_)
          vim.lsp.buf.format({ async = false })
        end, { desc = 'Format current buffer with LSP' })

        -- Configure diagnostics display
        -- https://github.com/neovim/nvim-lspconfig/wiki/UI-customization#customizing-how-diagnostics-are-displayed
        vim.diagnostic.config(opts.diagnostics)

        -- https://github.com/neovim/nvim-lspconfig/wiki/UI-customization#change-diagnostic-symbols-in-the-sign-column-gutter
        -- local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
        local signs = icons.diagnostics
        for type, icon in pairs(signs) do
          local hl = "DiagnosticSign" .. type
          vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
        end

        -- You will likely want to reduce updatetime which affects CursorHold
        -- note: this setting is global and should be set only once
        -- https://github.com/neovim/nvim-lspconfig/wiki/UI-customization#show-line-diagnostics-automatically-in-hover-window
        -- vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]
        vim.api.nvim_create_autocmd("CursorHold", {
          buffer = bufnr,
          callback = function()
            local float_opts = {
              focusable = false,
              close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
              border = preferred_border,
              source = 'if_many',
              prefix = ' ',
              scope = 'cursor',
            }
            vim.diagnostic.open_float(nil, float_opts)
          end
        })
      end

      -- https://github.com/LazyVim/LazyVim/blob/a72a84972d85e5bbc6b9d60a0983b37efef21b8a/lua/lazyvim/plugins/lsp/init.lua#L104
      -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

      -- Ensure the servers above are installed
      local mason_lspconfig = require 'mason-lspconfig'

      mason_lspconfig.setup {
        ensure_installed = vim.tbl_keys(servers),
      }

      mason_lspconfig.setup_handlers {
        function(server_name)
          require('lspconfig')[server_name].setup {
            capabilities = capabilities,
            on_attach = on_attach,
            settings = servers[server_name],
            filetypes = (servers[server_name] or {}).filetypes,
          }
        end
      }

      -- Setup neovim lua configuration
      require('neodev').setup()
    end,
  },
}
