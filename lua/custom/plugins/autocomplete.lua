local icons = require('custom.helpers.icons')

return {
  -- {
  --   -- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-completion.md#default-config
  --   'echasnovski/mini.completion',
  --   opts = {
  --     window = {
  --       info = { border = vim.g.pref_border_style or 'single' },
  --       signature = { border = vim.g.pref_border_style or 'single' },
  --     },
  --   },
  -- },
  {
    -- https://github.com/hrsh7th/nvim-cmp
    "hrsh7th/nvim-cmp",
    enabled = true,
    version = false, -- last release is way too old
    event = "InsertEnter",
    dependencies = {
      { "hrsh7th/cmp-nvim-lsp" },
      { "hrsh7th/cmp-nvim-lua" },
      -- { "hrsh7th/cmp-buffer" },
      -- { "hrsh7th/cmp-path" },
      { "saadparwaiz1/cmp_luasnip" },
      { "L3MON4D3/LuaSnip" },
      { 'hrsh7th/cmp-nvim-lsp-document-symbol' },
      { 'hrsh7th/cmp-nvim-lsp-signature-help' },
      { 'hrsh7th/cmp-cmdline' },
      { 'FelipeLema/cmp-async-path' },
      { 'nat-418/cmp-color-names.nvim' },
      -- {
      --   'Jezda1337/nvim-html-css',
      --   dependencies = {
      --     "nvim-treesitter/nvim-treesitter",
      --     "nvim-lua/plenary.nvim",
      --   },
      --   config = true,
      -- },
      { 'mmolhoek/cmp-scss' },
      {
        -- https://github.com/roginfarrer/cmp-css-variables#installation--setup
        "roginfarrer/cmp-css-variables",
      },
      { 'onsails/lspkind.nvim' },
      { "davidsierradz/cmp-conventionalcommits" },
    },
    opts = function()
      vim.api.nvim_set_hl(0, "CmpGhostText", { link = "Comment", default = true })
      local cmp = require("cmp")
      local defaults = require("cmp.config.default")()
      local lspkind = require('lspkind')
      return {
        completion = {
          completeopt = "menu,menuone,noinsert",
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        mapping = cmp.mapping.preset.insert({
          ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
          ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
          ["<C-b>"] = cmp.mapping.scroll_docs(-4),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),
          -- ["<C-Space>"] = cmp.mapping.complete(),
          ["<C-e>"] = cmp.mapping.abort(),
          -- ["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ["<CR>"] = cmp.mapping(function(fallback)
            -- This little snippet will confirm with tab, and if no entry is selected, will confirm the first item
            if cmp.visible() then
              local entry = cmp.get_selected_entry()
              if not entry then
                cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
              else
                cmp.confirm()
              end
            else
              fallback()
            end
          end, { "i", "s", "c", }),

          ["<S-CR>"] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
          }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
        }),
        sources = cmp.config.sources({
          -- { name = "html-css" },
          { name = "nvim_lsp" },
          -- { name = "luasnip" },
          -- { name = "buffer" },
          { name = "async-path" },
          { name = 'conventionalcommits' },
          { name = 'nvim_lsp_signature_help' },
          { name = 'nvim_lsp_document_symbol' },
          { name = 'css-variables' },
          { name = 'color_names'},
        }),
        formatting = {
          -- format = function(_, item)
          --   local icons = require("custom.helpers.icons").kinds
          --   if icons[item.kind] then
          --     item.kind = icons[item.kind] .. item.kind
          --   end
          --   return item
          -- end,
          format = lspkind.cmp_format({
            -- https://github.com/onsails/lspkind.nvim#option-2-nvim-cmp
            mode = 'symbol_text',
            -- preset = 'codeicons',
            maxwidth = 75,
            ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
            -- symbol_map = icons.kinds,
          })
        },
        experimental = {
          ghost_text = {
            hl_group = "CmpGhostText",
          },
        },
        sorting = defaults.sorting,
        view = {
          entries = {
            -- name = 'native',
            -- selection_order = 'near_cursor',
          },
        },
      }
    end,
    init = function()
      local cmp = require('cmp')
      cmp.setup.filetype('gitcommit', {
        sources = cmp.config.sources({
          { name = 'git' },
        }),
      })
    end,
  },
}
