local layer_theme = vim.g.pref_colorscheme_layers
local icons = require('custom.helpers.icons')

return {
  -- Color schemes
  {
    -- https://github.com/EdenEast/nightfox.nvim#configuration
    "EdenEast/nightfox.nvim",
    enabled = true,
    opts = {
      transparent = true,
      inverse = {
        match_parens = true,
        visual = true,
        search = true,
      },
      modules = {
        neotree = true,
        telescope = true,
        treesitter = true,
        whichkey = true,
        lsp_saga = true,
        lsp_trouble = true,
        ["lazy.nvim"] = true,
        indent_blanklines = true,
        gitgutter = true,
        gitsigns = true,
        cmp = true,
        barbar = true,
        ["dap-ui"] = true,
        mini = true,
        native_lsp = true,
      },
    },
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
  },
  {
    'AlexvZyl/nordic.nvim',
    lazy = false,
    priority = 1000,
    config = function()
      require 'nordic'.load()
    end
  },
  "sainnhe/everforest",
  {
    -- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-base16.md#default-config
    'echasnovski/mini.base16',
    version = false,
    -- config = function()
    --   local palette = require('mini.base16')
    --   palette.setup({ palette = palette.mini_palette('#001e1e', '#cccccc', 50) })
    -- end,
  },

  {
    -- https://github.com/eliseshaffer/darklight.nvim#colorscheme
    'eliseshaffer/darklight.nvim',
    lazy = false,
    opts = {
      mode = 'colorscheme',
      light_mode_colorscheme = vim.g.pref_colorscheme_light or 'nordic',
      dark_mode_colorscheme = vim.g.pref_colorscheme_dark or 'nordic',
    },
    config = true,
  },

  {
    -- https://github.com/f-person/auto-dark-mode.nvim#using-lazy
    "f-person/auto-dark-mode.nvim",
    config = {
      update_interval = 1000,
      set_dark_mode = function()
        vim.api.nvim_set_option("background", "dark")
        vim.cmd("colorscheme " .. vim.g.pref_colorscheme_dark)
      end,
      set_light_mode = function()
        vim.api.nvim_set_option("background", "light")
        vim.cmd("colorscheme " .. vim.g.pref_colorscheme_light)
      end,
    },
  },

  -- Other UI
  {
    -- Breadcrumbs dropbar (like IntelliJ's): Requires nvim 0.10+
    -- https://github.com/Bekaboo/dropbar.nvim#configuration
    'Bekaboo/dropbar.nvim',
    enabled = false, -- vim.api.nvim_eval("has('nvim-0.10.0')") == 1,
    dependencies = {
      'nvim-tree/nvim-web-devicons',
      'nvim-telescope/telescope-fzf-native.nvim',
    },
    opts = {
      sources = {
        path = {
          relative_to = function(_)
            return vim.fn.getcwd()
          end,
        },
      },
    },
    init = function()
      vim.ui.select = require('dropbar.utils.menu').select
    end,
  },
  {
    "LunarVim/breadcrumbs.nvim",
    enabled = vim.api.nvim_eval("has('nvim-0.10.0')") == 0,
    dependencies = {
      "SmiteshP/nvim-navic",
      opts = {
        lsp = {
          auto_attach = true,
        },
      },
    },
    config = true,
  },
  { "folke/twilight.nvim" },
  { "folke/zen-mode.nvim" },
  {
    -- Alt Nvim UI Components
    -- https://github.com/folke/noice.nvim#%EF%B8%8F-configuration
    "folke/noice.nvim",
    enabled = true,
    event = 'VeryLazy',
    opts = {
      -- cmdline = { enabled = false },
      signature = { enabled = false },
      lsp = {
        -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
        override = {
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
          ["cmp.entry.get_documentation"] = true,
        },
      },
      -- you can enable a preset for easier configuration
      presets = {
        bottom_search = true,         -- use a classic bottom cmdline for search
        command_palette = true,       -- position the cmdline and popupmenu together
        long_message_to_split = true, -- long messages will be sent to a split
        inc_rename = false,           -- enables an input dialog for inc-rename.nvim
        lsp_doc_border = false,       -- add a border to hover docs and signature help
      },
    },
    dependencies = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      -- OPTIONAL:
      --   `nvim-notify` is only needed, if you want to use the notification view.
      --   If not available, we use `mini` as the fallback
      -- "rcarriga/nvim-notify",
    },
  },
  {
    -- https://github.com/folke/styler.nvim#readme
    "folke/styler.nvim",
    enabled = false,
    -- lazy = true,
    opts = {
      themes = {
        help = { colorscheme = layer_theme },
        noice = { colorscheme = layer_theme },
        cmp = { colorscheme = layer_theme },
        neotree = { colorscheme = layer_theme },
        telescope = { colorscheme = layer_theme },
      },
    },
  },
  {
    'nvim-tree/nvim-tree.lua',
    lazy = false,
    keys = {
      { "<leader>fe", "<cmd>NvimTreeToggle<cr>", desc = "Toggle the File [E]xplorer" },
    },
    opts = {
      -- https://github.com/nvim-tree/nvim-tree.lua/blob/master/doc/nvim-tree-lua.txt
      filters = {
        git_ignored = false,
      },
      update_focused_file = {
        enable = true,
      },
      view = {
        signcolumn = 'auto',
        width = {
          min = 36,
          max = 50,
        },
      },
      renderer = {
        add_trailing = true,
        full_name = true,
        group_empty = true,
        highlight_git = true,
        highlight_diagnostics = true,
        icons = {
          git_placement = 'signcolumn',
          diagnostics_placement = 'signcolumn',
        },
      },
    },
    dependencies = {
      { 'nvim-tree/nvim-web-devicons' },
      {
        'antosha417/nvim-lsp-file-operations',
        dependencies = {
          "nvim-lua/plenary.nvim",
        },
        config = true,
      },
    },
    init = function()
      -- disable netrw at the very start of your init.lua
      vim.g.loaded_netrw = 1
      vim.g.loaded_netrwPlugin = 1

      -- set termguicolors to enable highlight groups
      vim.opt.termguicolors = true

      vim.api.nvim_create_autocmd("QuitPre", {
        callback = function()
          local tree_wins = {}
          local floating_wins = {}
          local wins = vim.api.nvim_list_wins()
          for _, w in ipairs(wins) do
            local bufname = vim.api.nvim_buf_get_name(vim.api.nvim_win_get_buf(w))
            if bufname:match("NvimTree_") ~= nil then
              table.insert(tree_wins, w)
            end
            if vim.api.nvim_win_get_config(w).relative ~= '' then
              table.insert(floating_wins, w)
            end
          end
          if 1 == #wins - #floating_wins - #tree_wins then
            -- Should quit, so we close all invalid windows.
            for _, w in ipairs(tree_wins) do
              vim.api.nvim_win_close(w, true)
            end
          end
        end
      })
    end
  },
  {
    -- https://github.com/mrjones2014/legendary.nvim#configuration
    'mrjones2014/legendary.nvim',
    priority = 99999,
    lazy = false,
    opts = {
      -- https://github.com/mrjones2014/legendary.nvim/blob/master/doc/EXTENSIONS.md
      extensions = {
        lazy_nvim = true,
        smart_splits = {},
        diffview = true,
        which_key = { auto_register = true },
      },
      -- https://github.com/mrjones2014/legendary.nvim/blob/HEAD/doc/table_structures/AUTOCMDS.md
      autocmds = require('custom.autocommands'),
    },
  },
  {
    "folke/which-key.nvim",
    dependencies = {
      'anuvyklack/keymap-amend.nvim',
    },
    -- event = "VeryLazy",
    lazy = false,
    priority = 88888,
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  },
  {
    -- https://github.com/romgrk/barbar.nvim#options
    'romgrk/barbar.nvim',
    enabled = true,
    dependencies = {
      'lewis6991/gitsigns.nvim',     -- OPTIONAL: for git status
      'nvim-tree/nvim-web-devicons', -- OPTIONAL: for file icons
    },
    init = function() vim.g.barbar_auto_setup = false end,
    opts = {
      -- lazy.nvim will automatically call setup for you. put your options here, anything missing will use the default:
      animation = true,
      clickable = true,
      tabpages = false,
      auto_hide = false,
      -- hide = { extensions = false, inactive = false },
      highlight_inactive_file_icons = false,
      highlight_alternate = false,
      icons = {
        diagnostics = {
          [vim.diagnostic.severity.ERROR] = { enabled = true, icon = icons.diagnostics.Error },
          [vim.diagnostic.severity.WARN] = { enabled = false, icon = icons.diagnostics.Warn },
          [vim.diagnostic.severity.INFO] = { enabled = false, icon = icons.diagnostics.Info },
          [vim.diagnostic.severity.HINT] = { enabled = false, icon = icons.diagnostics.Hint },
        },
        filetype = { enabled = true },
        preset = 'default',
        button = '×',
        buffer_index = false,
        current = { buffer_index = false },
      },
      sidebar_filetypes = {
        -- Use the default values: {event = 'BufWinLeave', text = nil}
        NvimTree = true,
        -- -- Or, specify the text used for the offset:
        -- undotree = { text = 'undotree' },
        -- Or, specify the event which the sidebar executes when leaving:
        -- ['neo-tree'] = { event = 'BufWipeout' },
        -- Or, specify both
        -- Outline = { event = 'BufWinLeave', text = 'symbols-outline' },
      },
      maximum_padding = 2,
      minimum_padding = 1,
      maximum_length = 20,
      no_name_title = '✨',
      semantic_letters = true,
      -- insert_at_start = true,
    },
    version = '^1.0.0', -- optional: only update when a new 1.x version is released
  },
  {
    'echasnovski/mini.map',
    dependencies = {
      'nvim-tree/nvim-web-devicons',
      'lewis6991/gitsigns.nvim',
    },
    config = function()
      local minimap = require('mini.map')
      minimap.setup({
        integrations = {
          minimap.gen_integration.builtin_search(),
          minimap.gen_integration.diagnostic(),
          minimap.gen_integration.gitsigns(),
        },
        symbols = {
          encode = minimap.gen_encode_symbols.dot('4x2'),
        },
        window = {
          width = 25,
          winblend = 7,
        },
      })
    end,
  },
  {
    -- https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-starter.md#default-config
    "echasnovski/mini.starter",
    version = false, -- wait till new 0.7.0 release to put it back on semver
    event = "VimEnter",
    opts = function()
      local logo = ''

      if vim.fn.executable('cfonts') == 1 then
        local cwd = vim.fn.getcwd()
        local phrase = string.match(cwd, '[/\\]([^/\\]+)$')
        local fonts = { 'chrome', 'block', 'tiny' }
        local figfonts = { 'sblood', 'rounded' }
        local banner = phrase
        local cfonts_cmd = "cfonts " .. phrase .. " -f " .. fonts[1]
        local fig_cmd = 'figlet -W -f' .. figfonts[1] .. ' ' .. phrase
        local exe_expr = "!" .. cfonts_cmd
        -- banner = vim.system({ 'cfonts', phrase }, { text = true }):wait().stdout
        -- banner = vim.system({ 'cfonts', phrase, '-f', fonts[1] }, { text = true }):wait().stdout
        banner = vim.api.nvim_exec("execute '" .. exe_expr .. "'", { output = true })
        logo = table.concat({
          'Project: ',
          string.gsub(banner, "(\r?\n)%s*\r?\n", "%1"),
          cwd,
        }, "\n")
      end
      local pad = string.rep(" ", 0)
      -- pad = ""
      local new_section = function(name, action, section)
        return { name = name, action = action, section = pad .. section }
      end

      local starter = require("mini.starter")
      --stylua: ignore
      local config = {
        evaluate_single = true,
        header = logo,
        items = {
          starter.sections.recent_files(5, true, false),
          new_section("Find file", "Telescope find_files", "Telescope"),
          new_section("Recent files", "Telescope oldfiles", "Telescope"),
          new_section("Grep text", "Telescope live_grep", "Telescope"),
          -- starter.sections.telescope(),
          new_section("Neovim config", "e $MYVIMRC", "Config"),
          new_section("Lazy", "Lazy", "Config"),
          new_section("New file", "ene | startinsert", "Built-in"),
          new_section("Quit", "qa", "Built-in"),
          -- starter.sections.builtin_actions(),
          new_section("Session restore", [[lua require("persistence").load()]], "Session"),
        },
        content_hooks = {
          starter.gen_hook.adding_bullet(pad .. "░ ", false),
          starter.gen_hook.aligning("center", "center"),
        },
      }
      return config
    end,
    config = function(_, config)
      -- close Lazy and re-open when starter is ready
      if vim.o.filetype == "lazy" then
        vim.cmd.close()
        vim.api.nvim_create_autocmd("User", {
          pattern = "MiniStarterOpened",
          callback = function()
            require("lazy").show()
          end,
        })
      end

      local starter = require("mini.starter")
      starter.setup(config)

      vim.api.nvim_create_autocmd("User", {
        -- pattern = "LazyVimStarted",
        pattern = "VimEnter",
        callback = function()
          local stats = require("lazy").stats()
          local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
          local pad_footer = string.rep(" ", 8)
          starter.config.footer = pad_footer .. "⚡ Neovim loaded " .. stats.count .. " plugins in " .. ms .. "ms"
          pcall(starter.refresh)
        end,
      })

      vim.api.nvim_create_user_command(
        'Dashboard',
        function()
          require('mini.starter').open()
        end,
        {}
      )
      vim.keymap.set({ 'n' }, '<Leader>SS', function()
        require('mini.starter').open()
      end, { desc = '[S]how [S]tart screen/Dashboard' })
    end,
  },
  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = true,
        -- theme = 'powerline_dark',
        -- theme = 'PaperColor',
        component_separators = '/',
        -- section_separators = '',
      },
      sections = {
        lualine_a = {
          {
            'mode',
            icons_enabled = true,
            fmt = function(str)
              return icons.modes[str:lower()] or str:sub(1, 2)
            end
          }
        },
      },
    },
  },
  {
    -- https://github.com/akinsho/toggleterm.nvim#setup
    'akinsho/toggleterm.nvim',
    opts = {
      size = function(term)
        if term.direction == "horizontal" then
          -- return vim.o.rows * 0.2
          return 12
        elseif term.direction == "vertical" then
          return vim.o.columns * 0.4
        end
      end,
    },
  },
  {
    -- https://github.com/ellisonleao/glow.nvim#setup
    "ellisonleao/glow.nvim",
    dependencies = {
      {
        -- https://github.com/richardbizik/nvim-toc#configuration
        'richardbizik/nvim-toc',
        -- config = true,
        ft = { 'markdown' },
        init = function()
          vim.api.nvim_create_autocmd(
            "BufEnter",
            {
              pattern = "*.md,*.markdown",
              callback = function()
                -- create command to generate table of contents for markdown files at current cursor position
                vim.api.nvim_buf_create_user_command(0, 'TOC',
                  function()
                    local toc = require('nvim-toc').generate_md_toc()
                    local line = vim.api.nvim_win_get_cursor(0)[1]
                    vim.api.nvim_buf_set_lines(0, line, line, true, toc)
                  end,
                  { nargs = 0 }
                )
              end
            }
          )
        end,
      },
    },
    opts = {
      border = vim.g.pref_border_style,
      style = 'dark',
    },
    config = true,
    cmd = "Glow",
  },
  {
    -- https://github.com/mrjones2014/smart-splits.nvim#configuration
    'mrjones2014/smart-splits.nvim',
    dependencies = {
      {
        -- https://github.com/kwkarlwang/bufresize.nvim#default-configuration
        "kwkarlwang/bufresize.nvim",
        lazy = false,
      },
    },
    opts = {
      ignored_filetypes = { "NvimTree", "NeoTree" },
      log_level = "warn",
      resize_mode = {
        hooks = {
          on_leave = function()
            return require('bufresize').register
          end,
        },
      },
    },
  },
}
