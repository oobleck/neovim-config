local prettierd = function()
  return {
    exe = "prettierd",
    args = { "--stdin-filepath", vim.fn.shellescape(vim.api.nvim_buf_get_name(0)), "--single-quote" },
    stdin = true,
  }
end

return {
  {
    "ThePrimeagen/refactoring.nvim",
    requires = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    config = true,
  },
  {
    -- https://github.com/mhartington/formatter.nvim#configure
    'mhartington/formatter.nvim',
    enabled = false,
    config = function()
      local util = require('formatter.util')

      require('formatter').setup({
        logging = true,
        log_level = vim.log.levels.WARN,
        filetype = {
          typescript = {
            require('formatter.filetypes.typescriptreact').prettierd,
          },
          javascript = {
            require('formatter.filetypes.typescriptreact').prettierd,
          },
          typescriptreact = {
            require('formatter.filetypes.typescriptreact').prettierd,
          },
          javascriptreact = {
            require('formatter.filetypes.typescriptreact').prettierd,
          },
          markdown = {
            require('formatter.filetypes.markdown').prettierd,
          },
          html = {
            require('formatter.filetypes.html').prettierd,
          },
          scss = {
            require('formatter.filetypes.css').prettierd,
          },
          css = {
            require('formatter.filetypes.css').prettierd,
          },
          json = {
            require('formatter.filetypes.json').prettierd,
          },
          yaml = {
            require('formatter.filetypes.yaml').yamlfmt,
          },
          svelte = {
            require('formatter.filetypes.svelte').prettierd,
          },
          lua = {
            require("formatter.filetypes.lua").stylua,
          },
          sh = {
            require("formatter.filetypes.sh").shfmt,
          },
          rust = {
            require('formatter.filetypes.rust').rustfmt,
          },
          -- Use the special "*" filetype for defining formatter configurations on
          -- any filetype
          ["*"] = {
            -- "formatter.filetypes.any" defines default configurations for any
            -- filetype
            require("formatter.filetypes.any").remove_trailing_whitespace,
          }
        },
      })

      -- vim.api.nvim_create_autocmd('BufWritePre', {
      --   pattern = '*',
      --   callback = function()
      --     vim.cmd([[Format]])
      --   end,
      -- })
    end,
  },
}
