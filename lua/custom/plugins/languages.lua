return {
  {
    'wuelnerdotexe/vim-astro',
    dependencies = {
      { 'virchau13/tree-sitter-astro' },
    },
    init = function()
      vim.g.astro_typescript = 'enable'
    end,
    config = false,
  },
  {
    "davidmh/mdx.nvim",
    config = true,
    dependencies = { "nvim-treesitter/nvim-treesitter" }
  },
  {
    -- https://github.com/leafOfTree/vim-svelte-plugin#configuration
    'leafOfTree/vim-svelte-plugin',
    config = function()
      local prefix = 'vim_svelte_plugin'
      local settings = {
        load_full_syntax = 0,
        use_typescript = 1,
        use_sass = 1,
        has_init_indent = 1,
        use_foldexpr = 1,
        debug = 0,
      }

      for prop_name, prop_val in ipairs(settings) do
        print(prefix.prop_name)
        vim.g[prefix.prop_name] = prop_val
      end
    end,
  }
}
