local icons = require('custom.helpers.icons')

return {
  { 'tpope/vim-fugitive' },
  { 'tpope/vim-rhubarb' },
  {
    "kdheepak/lazygit.nvim",
    cmd = {
      "LazyGit",
      "LazyGitConfig",
      "LazyGitCurrentFile",
      "LazyGitFilter",
      "LazyGitFilterCurrentFile",
    },
    -- optional for floating window border decoration
    dependencies = {
      "nvim-telescope/telescope.nvim",
      "nvim-lua/plenary.nvim",
    },
    init = function()
      -- https://github.com/kdheepak/lazygit.nvim#usage
      vim.g.lazygit_floating_window_winblend = vim.g.pref_blend
      vim.g.lazygit_floating_window_use_plenary = 1
    end,
    config = function()
      require("telescope").load_extension("lazygit")
    end,
  },
  {
    -- https://github.com/tanvirtin/vgit.nvim
    'tanvirtin/vgit.nvim',
    enabled = true,
    requires = {
      'nvim-lua/plenary.nvim'
    },
    config = true,
  },
  {
    -- https://github.com/chrisgrieser/nvim-tinygit?tab=readme-ov-file#configuration
    "chrisgrieser/nvim-tinygit",
    enabled = false,
    ft = { "gitrebase", "gitcommit" }, -- so ftplugins are loaded
    dependencies = {
      "stevearc/dressing.nvim",
      "nvim-telescope/telescope.nvim", -- either telescope or fzf-lua
      -- "ibhagwan/fzf-lua",
      "rcarriga/nvim-notify",      -- optional, but will lack some features without it
    },
  },
  {
    -- https://github.com/sindrets/diffview.nvim#configuration
    'sindrets/diffview.nvim',
    opts = {
      file_panel = {
        -- listing_style = "list",
      },
    },
    config = function (opts)
      vim.keymap.set({'n'}, "<Leader>gd", "<cmd>DiffviewOpen<cr>", {desc = "Open [G]it [d]iff view"})
      -- vim.keymap.set({'n'}, "<Leader>gD", "<cmd>DiffviewClose<cr>", {desc = "Close [G]it [d]iff view"})
      require("diffview").setup(opts)
    end
  },
  -- {
  --   'f-person/git-blame.nvim',
  --   opts = {
  --     -- https://github.com/f-person/git-blame.nvim#configuration
  --     enabled = true,
  --     date_format = '%r',
  --     virtual_text_column = vim.o.textwidth,
  --     delay = 500,
  --     message_template = '  ✎ <author> • <date> • <summary> • <sha>',
  --     message_when_not_committed = '  ✎ Not committed yet'
  --   },
  --   config = true,
  -- },
  {
    -- https://github.com/lewis6991/gitsigns.nvim#installation--usage
    -- Adds git related signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    opts = {
      -- See `:help gitsigns.txt`
      -- signs = {
      --   add = { text = icons.git.added },
      --   change = { text = icons.git.modified },
      --   delete = { text = icons.git.removed },
      --   changedelete = { text = icons.git.modified },
      -- },
      signs = {
        add = { text = '+' },
        change = { text = '△' },
        -- changedelete = { text = '△' },
        changedelete = { text = '⌦' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        -- untracked = { text = '' },
      },
      signcolumn = true,
      numhl = false,
      linehl = false,
      word_diff = false,
      current_line_blame = true,
      current_line_blame_opts = {
        -- virt_text_pos = 'right_align',
        ignore_whitespace = true,
        delay = 500,
      },
      current_line_blame_formatter_opts = {
        relative_time = true,
      },
      attach_to_untracked = false,
      -- https://create.roblox.com/docs/reference/engine/libraries/os#date
      current_line_blame_formatter = ' ✎ <author>, <author_time:%R> (<author_time:%b %d, %Y>) - <summary>',
      -- sign_priority = 100,
      on_attach = function(bufnr)
        vim.keymap.set('n', '<leader>gp', require('gitsigns').prev_hunk,
          { buffer = bufnr, desc = '[G]o to [P]revious Hunk' })
        vim.keymap.set('n', '<leader>gn', require('gitsigns').next_hunk, { buffer = bufnr, desc = '[G]o to [N]ext Hunk' })
        vim.keymap.set('n', '<leader>ph', require('gitsigns').preview_hunk, { buffer = bufnr, desc = '[P]review [H]unk' })
      end,
    },
    config = true,
  },
  {
    -- https://github.com/isak102/telescope-git-file-history.nvim#configuration
    "isak102/telescope-git-file-history.nvim",
    dependencies = {
      "tpope/vim-fugitive",
      "nvim-telescope/telescope.nvim",
    },
    config = function ()
      local ts = require("telescope")
      ts.load_extension("git_file_history")
      vim.keymap.set('n', '<Leader>gh', ts.extensions.git_file_history.git_file_history, { desc = "Show [G]it file [h]istory"})
    end
  },
}
