
-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
return {
  {
    -- https://github.com/klen/nvim-config-local
    "klen/nvim-config-local",
    opts = {
      -- Default options (optional)

      -- Config file patterns to load (lua supported)
      config_files = { ".nvim.lua", ".nvimrc", ".exrc" },

      -- Where the plugin keeps files data
      hashfile = vim.fn.stdpath("data") .. "/config-local",

      autocommands_create = true,   -- Create autocommands (VimEnter, DirectoryChanged)
      commands_create = true,       -- Create commands (ConfigLocalSource, ConfigLocalEdit, ConfigLocalTrust, ConfigLocalIgnore)
      silent = false,               -- Disable plugin messages (Config loaded/ignored)
      lookup_parents = false,       -- Lookup config files in parent directories
    },
    config = true,
  },
  {
    -- https://github.com/ahonn/vim-fileheader
    'ahonn/vim-fileheader',
    enabled = false,
    config = function()
      vim.notify('Configuring vim-fileheader')
      vim.g.fileheader_auto_add = 0
      vim.g.fileheader_auto_update = 0
      vim.g.fileheader_show_email = 0
      vim.g.fileheader_by_git_config = 0
    end
  },
}
