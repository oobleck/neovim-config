if vim.g.neovide then
  -- Helper function for transparency formatting
  local alpha = function()
    return string.format("%x", math.floor(255 * (vim.g.transparency or 0.8)))
  end
  -- Put anything you want to happen only in Neovide herei
  -- vim.o.guifont = "SauceCodePro Nerd Font:h13"
  vim.o.guifont = "JetBrainsMono Nerd Font:h13:#e-subpixelantialias"
  vim.g.neovide_confirm_quit = true

  -- g:neovide_transparency should be 0 if you want to unify transparency of content and title bar.
  vim.g.neovide_transparency = 0.0
  vim.g.transparency = 0.9
  vim.g.neovide_background_color = "#0f1117" .. alpha()
  vim.g.neovide_floating_blur_amount_x = 2.0
  vim.g.neovide_floating_blur_amount_y = vim.g.neovide_floating_blur_amount_x
  vim.g.neovide_underline_automatic_scaling = true

  -- follow system theme setting
  vim.g.neovide_theme = 'auto'

  -- Keymaps
  vim.keymap.set({ 'n', 'i', 's' }, '<D-v>', '<esc>]P<cr>', { desc = 'Paste before cursor (and align)' })
  vim.keymap.set({ 'n', 'i' }, '<D-s>', '<cmd>:update<cr>', { desc = "Save current buffer/file" })
  -- vim.keymap.set({ 'n', 'i' }, '<D-M-s>', '<cmd>:bufdo update<cr>', { desc = "Save all unsaved buffers/files" })
end

