local globals = {
  loaded_netrw = 1,
  loaded_netrwPlugin = 1,

  -- [[ Custom globals prefixed with "pref_" ]]
  pref_tab_order = 'BufferOrderByDirectory',
  pref_border_style = 'single',
  pref_blend = 0,
  -- pref_colorscheme_layers = 'nordic',
  pref_colorscheme_layers = "duskfox",
  -- pref_colorscheme_light = "catppuccin-latte",
  -- pref_colorscheme_dark = "retrobox",
  pref_colorscheme_light = "dawnfox",
  pref_colorscheme_dark = "terafox",
  pref_colorscheme = 'terafox',
  pref_colorscheme_alt = "nordfox",
  pref_autoformat = false,
}

for opt, val in pairs(globals) do
  vim.g[opt] = val
end



-- See `:help vim.o`
local opts = {
  shiftwidth = 2,
  hlsearch = false,
  mouse = 'a',
  clipboard = 'unnamedplus',
  breakindent = true,
  undofile = true,
  ignorecase = true,
  smartcase = true,
  updatetime = 500,
  timeoutlen = 300,
  -- Set completeopt to have a better completion experience
  completeopt = 'longest,menuone,noselect',
  winblend = vim.g.pref_blend or 0,
  pumblend = vim.g.pref_blend or 0,
  tabstop = 2,
  expandtab = true,
  wrap = false,
  termguicolors = true,
  number = true,
  relativenumber = false,
  textwidth = 80,

  -- Draw the column on the right side of the o.textwidth, not under it.
  colorcolumn = '80,120',
  -- colorcolumn = '81,121',
  -- colorcolumn = '+1,+41',
  -- Keep the current line vertically centered when scrolling or jumping in
  -- a buffer.
  scrolloff = 6,

  -- Code folding
  -- foldmethod = 'expr',
  -- foldexpr = 'nvim_treesitter#foldexpr()',

  formatoptions = "ctqlwanjp",
}

-- Set options from table
for opt, val in pairs(opts) do
  vim.o[opt] = val
end

