local tb = require('telescope.builtin')

function kmap(keys, cmd, desc, mode)
  vim.keymap.set(mode or 'n', keys, cmd, { desc = desc, noremap = true})
end

--[[ This file contains general and specific plugin keymaps. ]]

-- Tab/Shift+Tab indenting in most modes.
kmap('<Tab>', '>>_')
vim.keymap.set('n', '<S-Tab>', '<<_')
vim.keymap.set('v', '<Tab>', '>gv')
vim.keymap.set('v', '<S-Tab>', '<gv')
vim.keymap.set('i', '<S-Tab>', '<C-D>')

vim.keymap.set({ "n" }, 'gp', '<Nop>', { noremap = true, silent = true })
vim.keymap.set({ 'n' }, '<cr>', 'pumvisible() ? "<C-y>" : "<C-g>u<cr>"', { expr = true })
-- vim.keymap.set({ 'n' }, '<esc>', 'pumvisible() ? "<C-e>" : "<esc>"', { expr = true })

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.keymap.set({ 'n', 'i' }, '<C-S>', '<ESC><cmd>wa<CR>', { desc = '[S]ave all modified buffers' })
vim.keymap.set('n', '<Leader>qq', '<CMD>qa<CR>', { desc = '[Q]uit vim' })
kmap('<Del>', '"_d', 'Delete selection without copy', {'n', 'v'})
kmap('<BS>', '"_d', 'Delete selection without copy', {'n', 'v'})
-- kmap('<Del>', '<cmd>"_d', 'Delete selection without copy', {'i'})
kmap('<C-a>', 'ggVG', 'Select all lines', {'n', 'i'})

-- Uses Mini.bufremove's :bdelete
vim.keymap.set('n', '<Leader>x', '<CMD>bdelete<CR>', { desc = '[B]uffer [d]elete (close buffer)' })
vim.keymap.set('n', '<Leader>xx', '<CMD>bufdo bdelete<CR>', { desc = '[B]uffer [d]elete All (close all buffers)' })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- VSCode like palette triggers.
vim.keymap.set('n', '<Leader>p', '<cmd>Telescope find_files<cr>', { desc = 'Open File palette (vscode)' })
vim.keymap.set('n', '<Leader>P', ':', { desc = 'Open Command palette (vscode)' })

-- Mini.nvim keymaps
local minimap = require('mini.map');
-- vim.keymap.set('n', '<Leader>fe', toggle_explorer, { desc = 'Toggle Mini[F]ile [e]xplorer', silent = true })
vim.keymap.set('n', '<Leader>mc', minimap.close, { desc = 'Minimap: [C]lose' })
-- vim.keymap.set('n', '<Leader>mf', MiniMap.toggle_focus, {desc = ''})
-- vim.keymap.set('n', '<Leader>mo', MiniMap.open)
vim.keymap.set('n', '<Leader>mr', minimap.refresh, { desc = 'Minimap: [R]efresh' })
-- vim.keymap.set('n', '<Leader>ms', MiniMap.toggle_side)
vim.keymap.set('n', '<Leader>mt', minimap.toggle, { desc = 'Minimap: [T]oggle open/closed' })
vim.keymap.set('n', '<Leader>mm', minimap.toggle, { desc = 'Minimap: (alias) [T]oggle open/closed' })
-- [[ Code actioerver_path =s ]]
-- vim.keymap.set({'n'}, '<Leader>ca')
-- vim.keymap.set({ 'n' }, '<Leader>cf', '<CMD>Format<CR>', { silent = true, desc = '[F]ormat current Buffer' })

-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>?', tb.oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader><space>', tb.buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>/', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  tb.current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = vim.g.pref_blend,
    previewer = false,
  })
end, { desc = '[/] Fuzzy search in current buffer' })

vim.keymap.set('n', '<leader>gf', tb.git_files, { desc = 'Search [G]it [F]iles' })
vim.keymap.set('n', '<leader>sf', tb.find_files, { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', tb.help_tags, { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>sw', tb.grep_string, { desc = '[S]earch current [W]ord' })
vim.keymap.set('n', '<leader>sg', tb.live_grep, { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sd', tb.diagnostics, { desc = '[S]earch [D]iagnostics' })
vim.keymap.set('n', '<Leader>C', tb.colorscheme, { desc = 'Pick a Colorscheme' })


-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })


-- Code folding
-- vim.keymap.set('n', 'FF', '<cmd>za<cr>', { desc = 'Toggle code [F]olding' })

-- [[ Wrap symbol under cursor in <symbol> ]]
kmap('<C-W>', function()
  local wrap_char = vim.fn.input('Wrap with? ')
  vim.cmd('WrapSymbol ' .. wrap_char)
end, '[W]rap word under cursor')

-- kmap('<C-R><C-R>', '<cmd>ReloadConfig<cr>', '[R]reload nvim config')
kmap('<A-q>', '<esc>vipgw<cr>', 'Reflow current paragraph', {'n', 'v', 'i'})
